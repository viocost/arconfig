sudo pacman -R firefox --noconfirm

echo "
Xcursor.size: 8
" >> ~/.Xresources


echo "
gtk-cursor-theme-size=16
" >> ~/.config/gtk-3.0/setting.ini


echo "
%wheel ALL=(ALL) NOPASSWD: ALL
" | sudo tee -a /etc/sudoers.d/10-installer
