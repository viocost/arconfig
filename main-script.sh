#!/bin/bash

# Updating arch
echo Updating the system...
sleep 1
./run.sh "sudo pacman -Suy"

# Installing software
echo Installing software...
sleep 1
./run.sh ./software.sh

# Configuring git
echo Unpacking secrets...
sleep 1
./run.sh ./git.sh

# Installing fonts
echo Setting up fonts...
sleep 1
./run.sh ./fonts.sh

# Setting up symlinks
echo Wiring all together...
sleep 1
./run.sh ./symlinks.sh

# Setting up cron jobs
echo Spawning daemons...
sleep 1
./run.sh ./cron.sh
./run.sh ./services.sh

# Emacs
echo Setting up emacs...
sleep 1
./run.sh ./emacs.sh

read -p "Installation completed. Reboot now? (y/N)" answer
if [[ "${answer,,}" =~ ^y(es)?$ ]]; then
  sudo reboot now
else
  echo "Reboot cancelled"
fi
