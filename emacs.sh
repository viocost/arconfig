#!/bin/env bash
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.config/emacs

CURDIR=$(pwd)
if [[ ! -d ~/org-roam ]]; then
  git clone git@gitlab.com:viocost/kb.git -l ~/org-roam
fi

if [[ ! -d ~/.config/doom ]]; then
  git clone git@github.com:viocost/emacs-doom ~/.config/doom
fi

cd ~/.config/emacs/bin

$CURDIR/run.sh "./doom sync"
