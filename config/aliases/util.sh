alias freeram="~/.local/bin/scripts/freeram.sh"

alias CAPS="xdotool key Caps_Lock"

# rsync
alias rcp="rsync -ah --progress"
