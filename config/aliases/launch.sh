# neovim
alias vim="nvim"
alias v="nvim ."
alias v.="nvim ."

# arandr
alias arandr="devour arandr"

# zoom
alias zoom="devour ~/.local/bin/zoom"

alias tf="terraform"
