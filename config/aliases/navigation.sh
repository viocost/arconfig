alias al="vim ~/.config/aliases"
alias proj="cd ~/projects"
alias secrets="vim ~/.local/.secrets"
alias gc="cd $HOME/.config"

alias hosts="sudo nvim /etc/hosts"

alias i3conf="vim ~/.config/i3/config"
alias zconf="vim ~/.zshrc"
