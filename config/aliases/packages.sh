alias pci="sudo pacman -Sy --noconfirm"
alias pcr="sudo pacman -Runs --noconfirm"

alias pcs="pacman -Ss"
alias pcq="pacman -Qi"

alias yi="yay -Sy --noconfirm"
alias yr="yay -Runs --noconfirm"
alias yq="yay -Qi"
alias yl="yay -Qdt"
