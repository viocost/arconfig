#!/usr/bin/env bash

# Path to the directory containing the JPEG wallpaper file
wallpaper_dir="/home/kostia/wallpapers"

# Path to the Nitrogen executable
nitrogen_executable="nitrogen"

# Iterate over each screen and set wallpaper
for screen in $(xrandr --listmonitors | grep -oP '(?<=\s)[0-9]+(?=:)'); do
    # Get the wallpaper file name for the current screen
    wallpaper_file="${wallpaper_dir}/0305.jpg"

    # Check if the wallpaper file exists
    if [ -f "$wallpaper_file" ]; then
        # Set the wallpaper for the current screen using Nitrogen
        $nitrogen_executable --head=$screen --set-zoom-fill "$wallpaper_file"
        echo "Wallpaper set for screen $screen"
    else
        echo "Wallpaper file not found for screen $screen"
    fi
done
