#!/bin/env bash
# add fonts
# add russian keyboard
# switch Esc to caps

function makedir() {
  # Add scripts
  if [[ ! -d $1 ]]; then
    mkdir -p $1
  fi
}

function rmquiet() {
  rm $1 -rf 2>/dev/null
}

makedir ~/.local/bin
makedir ~/.config

rmquiet ~/.local/bin/scripts
ln -s $(pwd)/scripts $(readlink -f ~/.local/bin)

rmquiet ~/.zshrc
ln -s $(pwd)/zsh/zshrc $(readlink -f ~/.zshrc)

rmquiet ~/.config/aliasrc
ln -s $(pwd)/zsh/aliasrc $(readlink -f ~/.config)

rmquiet ~/.config/aliases
ln -s $(pwd)/config/aliases $(readlink -f ~/.config)
# i3
rmquiet ~/.config/i3
ln -s $(pwd)/config/i3 $(readlink -f ~/.config)

rmquiet ~/.xprofile
ln -s $(pwd)/config/i3/xprofile $(readlink -f ~/.xprofile)

rmquiet ~/.config/scripts
ln -s $(pwd)/scripts $(readlink -f ~/.config)

rmquiet ~/.config/rofi
ln -s $(pwd)/config/rofi $(readlink -f ~/.config)

rmquiet ~/.config/assets
ln -s $(pwd)/assets $(readlink -f ~/.config)

rmquiet ~/.config/nvim
cp -r $(pwd)/config/nvim $(readlink -f ~/.config)/nvim

rmquiet ~/.config/tmux
cp -r $(pwd)/config/tmux $(readlink -f ~/.config)/tmux

rmquiet ~/.config/kitty
ln -s $(pwd)/config/kitty $(readlink -f ~/.config)

rmquiet ~/.config/fastfetch
ln -s $(pwd)/config/fastfetch $(readlink -f ~/.config)

rmquiet ~/.path
ln -s $(pwd)/path.txt ~/.path
