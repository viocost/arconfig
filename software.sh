#!/bin/bash

# First arg is pacman or yay, second - name of the package
function install() {
  if [[ $1 == pacman ]]; then
    COMMAND="sudo $1 -S $2 --noconfirm"

  elif [[ $1 == pip ]]; then
    COMMAND="$1 install $2 "
  elif [[ $1 == yay ]]; then
    COMMAND="$1 -S $2 --noconfirm"
  fi
  $COMMAND
  while [ $? -ne 0 ]; do
    read -p "Error installing $1. Try again? Y/n: " yn

    case $yn in
    [Yy]*) $COMMAND ;;
    [Nn]*) break ;;
    *) echo "Please answer yes or no." ;;
    esac

  done
}

function makedir() {
  # Add scripts
  if [[ ! -d $1 ]]; then
    mkdir -p $1
  fi
}

function remove_conflicting() {
  pacman -Qi i3lock | grep i3lock && sudo pacman -Runs i3lock
}

pacman=(
  # apps
  neovim
  chromium
  openvpn
  docker
  docker-compose
  remmina
  emacs
  kitty
  tmux
  bitwarden
  obs-studio
  minikube
  qbittorrent
  pinta
  github-cli
  yazi # terminal file manager
  # Shows sysinfo in the new terminal
  fastfetch

  # utils
  cronie
  autorandr
  brightnessctl
  rust
  npm
  picom
  curl
  sqlite
  python-pip
  hunspell-en_us
  sysstat
  acpi
  lsof
  xdotool
  direnv
  zsh
  blueman
  xfce4-screenshooter

  # Font manager
  gucharmap

  # Wallpaper tool
  feh

  # for neovim
  tree-sitter
  tree-sitter-cli
  python-pynvim
  nodejs
  xclip

  # latex
  texlive-bin
  texlive-latexextra

  # This package used for auto-renaming workspaces
  python-i3ipc
  python-urllib3

  # Wireless tools
  iw
  wireless_tools

  # Required for auto-lock script
  xprintidle

)

remove_conflicting

# Install stuff
for i in "${pacman[@]}"; do
  install pacman $i
done

yay=(

  # Colorful lock screen
  i3lock-color

  # Steganography
  steghide

  # This is needed for auto-renaming workspaces
  python-fontawesome

  pcloud-drive
  devour

  slack-desktop

  drawio-desktop

  # Used for displaying keyboard layout in i3blocks in event-driven way
  xkb-switch-git
)

# Install stuff from yay
for i in "${yay[@]}"; do
  install yay $i
done

function docker_permissions() {
  sudo usermod -aG docker $USER
}

PROFILE=/dev/null bash -c 'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash'

function antigen() {
  makedir ~/.config/zsh
  curl -L git.io/antigen >~/.config/zsh/antigen.zsh

}

echo installing antigen
antigen

echo installing docker permissions
docker_permissions

echo setting up ohmyzsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

chsh -s $(which zsh)
